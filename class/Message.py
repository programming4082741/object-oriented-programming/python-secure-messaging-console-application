
class Message:
    def __init__(self, ref, obj, field, sent, sender, seen='n'):
        self.__ref = ref
        self.__obj = obj
        self.__field = field
        self.__sent = sent
        self.__sender = sender
        self.__seen = seen
    
    # Reference
    @property
    def ref(self):
        return self.__ref
    
    @ref.setter
    def uid(self, ref):
        self.__ref = ref
    
    # Object
    @property
    def obj(self):
        return self.__obj
    
    @obj.setter
    def obj(self, obj):
        self.__obj = obj
    
    # Message Field
    @property
    def field(self):
        return self.__field
    
    @field.setter
    def field(self, field):
        self.__field = field
    
    # Sending time
    @property
    def sent(self):
        return self.__sent
    
    @sent.setter
    def sent(self, sent):
        self.__sent = sent
    
    # Sender (user)
    @property
    def sender(self):
        return self.__sender
    
    @sender.setter
    def sender(self, sender):
        self.__sender = sender
    
    # Seen
    @property
    def seen(self):
        return self.__seen
    
    @seen.setter
    def seen(self, seen):
        self.__seen = seen