
class User:
    def __init__(self, uid, login, pwd, privilige):
        self.__uid = uid
        self.__login = login
        self.__pwd = pwd
        self.__privilige = privilige
    
    @property
    def uid(self):
        return self.__uid
    
    @uid.setter
    def uid(self, uid):
        self.__uid = uid
    
    @property
    def login(self):
        return self.__login
    
    @login.setter
    def login(self, login):
        self.__login = login
    
    @property
    def pwd(self):
        return self.__pwd
    
    @pwd.setter
    def pwd(self, pwd):
        self.__pwd = pwd