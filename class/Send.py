
class Send:
    def __init__(self, sendID, msgRef, receiver):
        self.__sendID = sendID
        self.__msgRef = msgRef
        self.__receiver = receiver

    # sendID
    @property
    def sendID(self):
        return self.__sendID
    
    @sendID.setter
    def sendID(self, sendID):
        self.__sendID = sendID
    
    # Message
    @property
    def msgRef(self):
        return self.__msgRef
    
    @msgRef.setter
    def msgRef(self, msgRef):
        self.__msgRef = msgRef
    
    # Receiver (User)
    @property
    def receiver(self):
        return self.__usr
    
    @receiver.setter
    def receiver(self, user):
        self.__receiver = user