CREATE DATABASE tpython;
USE tpython;


CREATE TABLE user(
uid INT AUTO_INCREMENT PRIMARY KEY,
login VARCHAR(50) NOT NULL,
pwd VARCHAR(50) NOT NULL,
privilige INT DEFAULT 1,
UNIQUE (login));
INSERT INTO user (login,pwd, privilige) VALUES ('admin', 'admin', 2);


CREATE TABLE `message` (
	`reference` VARCHAR(14) NOT NULL,
	`object` VARCHAR(50),
	`field` VARCHAR(200),
	`sent` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`sender` VARCHAR(50) NOT NULL,
	`seen` CHAR(1) NOT NULL DEFAULT 'N',
	PRIMARY KEY (`reference`)
);


CREATE TABLE send(
sendID INT AUTO_INCREMENT PRIMARY KEY,
msgRef VARCHAR(14) NOT NULL,
receiver VARCHAR(50) NOT NULL);