import mysql.connector

""" Connection """
db = mysql.connector.connect(
  user = "administrator",
  password = "gen_pwd",
  database = "tpython"
)

def insert(table: str, columns: tuple, vals=[()]):
    cursor = db.cursor()
    query = "INSERT INTO "+table+" "+str(columns).replace('\'','')+" VALUES (%"+ ", %".join('s'*len(columns))+ ")"
    cursor.executemany(query, vals)
    db.commit()
    print(cursor.rowcount, "record(s) inserted.")


def select(table: str, columns="*", conditions=""):
    cursor = db.cursor()
    if conditions=="":
        if(isinstance(columns, str)):
            query="SELECT " +columns+ " FROM "+table
        else:
            query="SELECT " +str(columns)[1:-1]+ " FROM "+table
    else:
        if(isinstance(columns, str)):
            query="SELECT " +columns+ " FROM "+table+" WHERE "+conditions
        else:
            query="SELECT " +str(columns)[1:-1].replace('\'','')+ " FROM "+table+" WHERE "+conditions
        cursor.execute(query)
        return cursor.fetchall()
    cursor.execute(query)
    rows = cursor.fetchall()
    print('Total Row(s):', cursor.rowcount)
    r=[]
    for row in rows:
        r.append(row)
        print(row)
    return r


def delete(table: str, conditions: str):
    query = "DELETE FROM "+table+ " WHERE "+conditions
    cursor = db.cursor()
    cursor.execute(query)
    db.commit()
    print(cursor.rowcount, "record(s) deleted")


def update(table: str, column: str, newval: str, conditions: str):
    cursor = db.cursor()
    query = "UPDATE "+table+" SET "+column+" = \""+newval+"\" WHERE "+conditions
    cursor.execute(query)
    db.commit()
    print(cursor.rowcount, "record(s) updated")