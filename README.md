# Secure Messaging Console Application

This Python program provides a user-friendly interface for creating, sending, and managing secure messages. Developed with an emphasis on user authentication and data privacy, this application allows users to perform various actions such as signing up, connecting, sending and receiving messages, and managing their accounts.

## Project Overview

### Components
- `os`: For system-related operations.
- `sysMsg` and `MySQL_cmd`: Custom modules for secure messaging and MySQL database interaction.
- `cmd`: User command input.
- `connected`: A flag to indicate the user's connection status.

### Functionality

The application offers the following functionalities:

1. **User Authentication**
   - **Sign Up**: Create a new account with a login and password.
   - **Connect**: Log in with an existing account.

2. **Secure Messaging**
   - **Show Mails**: View received messages.
   - **Read Mails**: Read a specific message.
   - **Write Mail**: Compose a new message.
   - **Send Mail**: Send a drafted message to one or more recipients.

3. **User Interaction**
   - **Show Menu**: Display the available menu options.
   - **Disconnect**: Log out and return to the initial menu.

### Menu of Options

The program provides a clear menu of options for the user, allowing them to perform various actions:

```
1. Sign Up            2. Connect            q. Quit
```

Once connected, users can access a submenu with additional options for secure messaging:

```
1. Show Mails         2. Read Mails         3. Write Mail         4. Send Mail
0. Show Menu          d. Disconnect
```

## User Interaction

Users interact with the program through the command-line interface, entering commands to perform actions. The program guides users through the process and provides feedback to ensure a smooth user experience.

## Project Goals

- Enable users to create and manage accounts securely.
- Facilitate secure messaging with options to read, write, and send messages.
- Offer a user-friendly, menu-driven interface.
- Ensure user privacy and data security through authentication.