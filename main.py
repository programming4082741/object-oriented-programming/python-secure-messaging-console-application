import os
import sysMsg as sm
import MySQL_cmd as msc

cmd = ''
connected = 1
while cmd!="q":
    
    # Connection loop
    while True:
        user_dict = {}
        message_dict = {}
        send_dict = {}
        login=''
        pwd=''
        
        cmd = input('1. Sign Up\t\t2. Connect\t\tq. Quit\n-> ')
        
        if cmd=="q":
            break
        
        if cmd=="1":
            login = input('Create account\nEnter login: ')
            pwd = input('Enter password: ')
            sm.signup_user(str(login), str(pwd), user_dict)
            print("Your account is created!")
            
        
        else:
            if cmd=="2":
                login = input('Enter login: ')
                pwd = input('Enter password: ')
                info = msc.select("user", conditions='login=\"'+login+'\" AND pwd=\"'+pwd+"\"")
            
                if not info:
                    print("No such user.Try again!")
                else:
                    uid = info[0][0]
                    login = info[0][1]
                    pwd = info[0][2]
                    privilige = info[0][3]
                    connected = 0
                    print("\n\n1. Show mails\t\t2. Read mails\n3. Write mail\t\t4. Send mail\n0. Show menu\t\td. Disconnect\n")
                    break
            else:
                print("Command not found")
    
    # Command loop
    while connected == 0:
        cmd = input('Enter command code: ')
        
        if cmd=="0":
            print("\n\n1. Show mails\t\t2. Read mails\n3. Write mail\t\t4. Send mail\n0. Show menu\t\td. Disconnect\n")
        
        if cmd=="d":
            connected = 1
            break
            
        if cmd=="1":
            tmp = sm.allMessages(login)
            for i in range(len(tmp)):
                ron='Not Read'
                if tmp[i][4]=='Y':
                    ron='Read'
                print('{}.\t{}\t{}\t{}\t{}\t{}\n'.format(i+1, tmp[i][0], tmp[i][1], tmp[i][2], tmp[i][3], ron))
            
        if cmd=="2":
            msgRef = input('Enter mail reference: ')
            tmp = sm.showMsg(msgRef)
            print('\n{}\t\t{}\nObject: {}\n\t{}\n\n{}\n'.format(tmp[0], tmp[3], tmp[1], tmp[2], tmp[4]))
        
        
        if cmd=="3":
            obj = input('Object: ')
            field = input('Message: \n')
            sm.writeMessage(obj, field, login, message_dict)
        
        
        if cmd=="4":
            old_stdout = sm.sys.stdout
            sm.sys.stdout = open(os.devnull, "w")
            tmp = sm.draft(login)
            sm.sys.stdout = old_stdout
            print(tmp)
            
            if len(tmp)==0:
                print("Empty draft")
            else:
                msgRef = input('Enter mail reference: ')
                n = input('Number of Receivers: ')
                receiver=[]
                for i in range(int(n)):
                    res = input('To: ')
                    receiver.append(res)
                receiver = tuple(receiver)
                sm.sendMessage(msgRef, receiver, send_dict)