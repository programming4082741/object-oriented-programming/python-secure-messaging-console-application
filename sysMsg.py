import sys
sys.path.insert(0, 'class/')
sys.path.insert(0, 'db/')

import MySQL_cmd as msc

import time
time.ctime()

from User import User
from Message import Message
from Send import Send


def signup_user(login, pwd, d):
    if(isinstance(login, tuple)):
        for i in range(len(login)):
            msc.insert("user", ("login","pwd"), [(login[i], pwd[i])])
            tmp = msc.select("user", conditions='login=\"'+login[i]+'\"')
            d["{}".format(login)] = User(tmp[0][0], tmp[0][1], tmp[0][2], tmp[0][3])
    else:
        msc.insert("user", ("login","pwd"), [(login, pwd)])
        tmp = msc.select("user", conditions='login=\"'+login+'\"')
        d["{}".format(login)] = User(tmp[0][0], tmp[0][1], tmp[0][2], tmp[0][3])


def writeMessage(obj, field, sender, d):
    ref = time.strftime("%Y%m%d%H%M%S")
    msc.insert("message",
               ("reference","object", "field", "sent", "sender"),
               [(ref, obj, field, time.strftime("%Y-%m-%d %H:%M:%S"), sender)])
    tmp = msc.select("message", conditions='reference=\"'+ref+'\"')
    d["{}".format(ref)] = Message(tmp[0][0], tmp[0][1], tmp[0][2], tmp[0][3], tmp[0][4])


def draft(user: str):
    unsent = []
    msg = msc.select("message", columns="reference", conditions='sender=\"'+user+"\"")
    sent = msc.select("send", columns="msgRef")
    
    if len(sent)==0:
        return msg
    
    for i in range(len(msg)):
        tmp=0
        for j in range(len(sent)):
            if(msg[i][0]==sent[j][0]):
                tmp=1
                break
        if tmp==0:
            unsent.append(msc.select("message", columns="reference",
                                     conditions='reference=\"'+msg[i][0]+"\"")[0] )
    return unsent


def sendMessage(msgRef, receiver, d):
    if(isinstance(receiver, tuple)):
        for i in range(len(receiver)):
            msc.insert("send", ("msgRef","receiver"), [(msgRef, receiver[i])])
            tmp = msc.select("send", conditions='msgRef=\"'+msgRef+'\"')
            d["{}".format(msgRef)] = Send(tmp[0][0], tmp[0][1], tmp[0][2])
    else:
        msc.insert("send", ("msgRef","receiver"), [(msgRef, receiver)])
        tmp = msc.select("send", conditions='msgRef=\"'+msgRef+'\"')
        d["{}".format(msgRef)] = Send(tmp[0][0], tmp[0][1], tmp[0][2])


def allMessages(user: str):
    r = msc.select("send", columns="msgRef", conditions='receiver=\"'+user+"\"")
    k = []
    for i in range(len(r)):
        k.append( msc.select("message",
                             columns=("reference", "object", "sent", "sender", "seen"),
                             conditions='reference=\"'+r[i][0]+"\"")[0] )
    return k



def showMsg(usrMsgRef):
    r = msc.select("message",
                        columns="reference, object, field, sent, sender",
                        conditions='reference=\"'+usrMsgRef+"\"")[0]
    msc.update(table="message",
               column="seen",
               newval="Y",
               conditions='reference=\"'+usrMsgRef+"\"")
    return r